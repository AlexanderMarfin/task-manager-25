package ru.tsc.marfin.tm.exception.system;

import ru.tsc.marfin.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
